import os
from urllib2 import urlopen
import re

urlpath =urlopen('http://archive.outernet.is/')
string = urlpath.read().decode('utf-8')

pattern = re.compile('[a-zA-Z0-9]+\.zip') #the pattern actually creates duplicates in the list

filelist = pattern.findall(string)

os.chdir('tmp/downloads/content/')

for filename in filelist:
    fullfilename1 = 'C:/Users/Alan/Documents/GitHub/librarian/tmp/zipballs/'+filename
    fullfilename2 = 'C:/Users/Alan/Documents/GitHub/librarian/tmp/downloads/content/'+filename
    if not os.path.isfile(fullfilename1) and not os.path.isfile(fullfilename2):
        print('Downloading: ' + filename)
        remotefile = urlopen('http://archive.outernet.is/' + filename)
        localfile = open(filename,'wb')
        localfile.write(remotefile.read())
        localfile.close()
        remotefile.close()