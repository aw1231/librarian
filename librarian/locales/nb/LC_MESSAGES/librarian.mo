��    3      �  G   L      h     i     }     �  "   �     �  %   �  *     3   3  5   g  '   �  	   �     �     �     �     �     �       #        7  
   V     a  "   }  "   �     �  T   �     8  
   @  -   K     y     ~     �     �     �     �     �     �     �       $     -   C     q     �  	   �     �     �     �     �     �  	   �     �  "  �     �	     
     
     1
     K
  .   h
  ,   �
  ?   �
  ?     /   D  	   t     ~     �     �     �     �     �  #   �     �  
   �     
  "   &  "   I     l  T   �  	   �  	   �  0   �     &     +     A     D     Y     r     �     �     �     �  $   �  .   �     %     9  	   K     U     c     l     q     w          �                !   
      *      1   #               )         3         .                       +               (      %                    ,   $   "      /   '      	                          0   &      2                             -                     Against DRM License All rights reserved Application logs Content library is currently empty Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Download application log Error Errors Files Free Art License Frequency must be a positive number GNU Free Documentation License Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Logs are shown in reverse chronological order None North America Ku band OK Open Content License Open Publication License Other free license Other non-free license Public Domain Script output Select a satellite Symbolrate must be a positive number There are currently no files or folders here. Tuner settings Under maintenance Universal Unknown license Vertical error next no lock no output previous Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: no
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=2; plural=(n != 1);
 Mot DRM-lisens Alle rettigheter Applikasjonslogger Innholdsbibliotek er tomt Creative Commons Navngivelse Creative Commons Navngivelse-IngenBearbeidelse Creative Commons Navngivelse-IkkeKommersiell Creative Commons Navngivelse-IkkeKommersiell-IngenBearbeidelser Creative Commons Navngivelse-IkkeKommersiell-DelPåSammeVilkår Creative Commons Navngivelse-DelPåSammeVilkår Custom... Slett Download application log Error Feil Filer Fri kunstlisens Frequency must be a positive number GNU fri dokumentasjonslisens Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Bibliotek Laster... Logger er vist i omvendt kronologisk rekkefølge None North America Ku band OK Åpen innholdslisens Åpen publikasjonslisens Annen gratis lisens Annen ikke gratis lisens Offentlig Domene Script output Select a satellite Symbolrate must be a positive number Det er for tiden ingen filer eller mapper her. Tuner-innstillinger Under maintenance Universal Ukjent lisens Vertical feil neste no lock ingen output forrige 