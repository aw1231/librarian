��    d      <  �   \      �     �     �  6   �     �     �     �     �     	     
	  "   	     >	  %   [	  *   �	  3   �	  5   �	  '   
  	   >
     H
     O
     _
     g
     p
     �
     �
     �
  	   �
     �
  	   �
  #   �
     �
       
          "   ;  "   ^     �     �     �  T   �       
             .     6  -   <  
   j     u     �     �     �     �     �     �     �     �     �               2     A  /   J  <   z     �     �     �     �     �     �                    0  $   <  #   a  0   �  -   �     �             U   -     �     �  	   �     �     �     �     �  1   �               $     *     /     4  	   <     F     O     X     `    i     �     �  6   �     �          
                    /     F  *   b  )   �  C   �  <   �  -   8     f     x     |     �     �     �     �     �     �     �     �  	   �  -   �     )     G     W  #   ]  &   �  '   �  #   �     �     �  U        \     h     w     �     �  4   �  
   �     �     �     �                    7     E     Q     i     �     �     �     �  6   �  .   
     9     F     R     Z     l     |     �     �     �     �  (   �     �        /   )     Y     x      �  U   �     �                    +     4     :  1   >     p     �     �     �     �     �     �     �     �  
   �     �         &      4       +      ]   F   /                  *      !          "      A   #   D   J   R      @       V       N       0   K      W   6   >       _   	   '       =   )   M      
   ;          Z   %   [       <      O   b      ,          C      H   Q                  X   ^          B       S       a   G          1   $   :   c   `                  \   9           Y   ?                     P   7          E           8       (   3                         I                -   U   .   L   d      T      5   2    Against DRM License All rights reserved An error occurred during the rendering of this plugin. Application logs Back C band Cancel Clear Confirm Password Content library is currently empty Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Delivery system Discard Download Download application log Error Errors Files Forbidden Free Art License Frequency Frequency must be a positive number GNU Free Documentation License Go up one level Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization LNB Type Language Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Log into Librarian Log out Login Logs are shown in reverse chronological order Modulation Network interfaces Next No No files are being downloaded None North America Ku band Notifications OK Open Content License Open Publication License Other free license Other non-free license Page not found Password Please enter the correct username and password. Please select the satellite you'd like to receive data from. Polarization Public Domain Quality Script output Select a satellite Settings Setup Completed Signal Start using Outernet Symbol rate Symbolrate must be a positive number The entered passwords do not match. The page you were looking for could not be found There are currently no files or folders here. There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. Tuner settings Under maintenance Universal Unknown license Username Vertical Yes You'll be taken to {target} in {seconds} seconds. any language confirm password error here next no lock no output password previous unknown username Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=2; plural=(n>1);
 DRM lisansı aleyhinde Tüm Hakları Saklıdır An error occurred during the rendering of this plugin. Uygulama kayıtları Back C band Cancel Clear Confirm Password Burada bir içerik yok Creative Commons Şartları Creative Commons Şartları-Paylaşılamaz Creative Commons Ticari Olmayan Kullanım Creative Commons Şartları-Ticari olmayan-paylaşılamayan içerik Creative Commons Şartları-Ticari olmadan Özgürce Paylaş Creative Commons Şartları-Özgürce Paylaş Özelleştirin... Sil Delivery system Discard İndir Uygulama kaydını indir Hata Hatalar Dosyalar Yetki gerekiyor Özgür Sanat Lisansı Frequency Frekans değeri pozitif bir sayı olmalıdır GNU Bedava Döküman Lisansı Go up one level Yatay LNB türü için geçersiz seçenek İletim sistemi için geçersiz seçim Modülasyon modu için geçersiz seçim Polarizasyon için geçersiz seçim LNB Type Language Librarian şu anda bakım modunda. Lütfen birkaç dakika bekleyin ve tekrar deneyin. Kütüphane Yükleniyor... Log into Librarian Çıkış yap Giriş Kayıtlar ters kronolojik sıraya göre gösterilir. Modulation Bağlantı arayüzleri Sonraki No İndirilen dosya yok Hiçbiri Kuzey Amerika Ku Bandı Notifications Başarılı Açık içerik lisansı Açık yayın lisansı Diğer bedava lisanslar Diğer bedava olmayan lisanslar Sayfa bulunamadı Password Lütfen geçerli bir kullanıcı adı ve şifre girin. Lütfen veri almak istediğiniz uyduyu seçin. Polarization Kamu Alanı Quality Komut çıktısı Bir uydu seçin Settings Kurulum tamamlandı Signal Start using Outernet Symbol rate Symbolrate pozitif bir sayı olmalıdır Girilen şifreler uyuşmuyor. Aradığınız sayfa bulunamadı Şu anda burada hiçbir dosya veya klasör yok. There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. Akor ayarları Bakımda Evrensel Bilinmeyen lisans Username Dikey Yes You'll be taken to {target} in {seconds} seconds. herhangi bir dil şifreyi onayla hata burada sonraki bağlı değil çıktı yok şifre önceki bilinmeyen kullanıcı adı 