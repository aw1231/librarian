��    ?        Y         p     q     �     �  "   �     �  %   �  *     3   ;  5   o  '   �  	   �     �     �     �                  	          #   .     R  
   q     |  "   �  "   �     �  T   �     S  
   [     f     n     t     �     �     �     �     �     �     �     	     	  /   *	     Z	     h	     v	  $   �	  0   �	  -   �	     
     
  	   .
     8
     H
     Q
     ^
     d
     i
  	   q
     {
     �
     �
     �
  "  �
     �     �     �     �       %   ,  *   R  3   }  5   �  '   �  	                  '     @     F     K  	   Q     [  #   l     �  
   �     �  "   �  "   �       T   <  	   �     �     �     �     �     �     �     �                    1     B     X  /   g     �     �     �  $   �  0   �     !     8     L  	   ^     h     v          �     �     �     �     �  
   �     �     �        0      %             "      >                         *                    '   2       $      9   /      <   )         1                                +   .          ,   3                 &   8          -       	   4              5                !          ;   7       :      6   (          
       #   =           ?    Against DRM License All rights reserved Application logs Content library is currently empty Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Download Download application log Error Errors Files Forbidden Free Art License Frequency must be a positive number GNU Free Documentation License Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Log out Login Network interfaces No files are being downloaded None North America Ku band OK Open Content License Open Publication License Other free license Other non-free license Page not found Please enter the correct username and password. Public Domain Script output Select a satellite Symbolrate must be a positive number The page you were looking for could not be found There are currently no files or folders here. Tuner settings Under maintenance Universal Unknown license Vertical any language error next no lock no output password previous unknown username Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=2; plural=(n != 1);
 Imod DRM licens Alle rettigheder reserveret  Program logs Biblioteket er tomt Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Slet Download Download application log Error Fejl Filer Forbidden Free Art License Frequency must be a positive number GNU Free Documentation License Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Bibliotek Indlæser... Log out Login Network interfaces No files are being downloaded None North America Ku band OK Åbn indholdslicens Åbn publikationslicens Anden fri licens Anden ikke-fri licens Page not found Please enter the correct username and password. Offentligt Domæne Script output Select a satellite Symbolrate must be a positive number The page you were looking for could not be found Der er ingen filer her Tuner indstillinger Under maintenance Universal Ukendt licens Vertical any language fejl næste no lock Intet output password forgående unknown username 