��    d      <  �   \      �     �     �  6   �     �     �     �     �     	     
	  "   	     >	  %   [	  *   �	  3   �	  5   �	  '   
  	   >
     H
     O
     _
     g
     p
     �
     �
     �
  	   �
     �
  	   �
  #   �
     �
       
          "   ;  "   ^     �     �     �  T   �       
             .     6  -   <  
   j     u     �     �     �     �     �     �     �     �     �               2     A  /   J  <   z     �     �     �     �     �     �                    0  $   <  #   a  0   �  -   �     �             U   -     �     �  	   �     �     �     �     �  1   �               $     *     /     4  	   <     F     O     X     `  l  i     �  "   �  6     !   E     g     l     s     z     �  4   �     �  &   �  +     4   7  6   l  (   �  	   �     �     �     �     �          %     2  
   >     I  K   \  	   �  Q   �          $     4  0   Q  "   �  <   �     �            �        �     �       
     
   "  a   -  
   �  #   �     �     �     �     �  5   �     -     ;     >     T  0   n  5   �  $   �     �  k     {   o     �     �               8     X  %   a     �     �     �  d   �  8     W   M  A   �     �             U   0     �     �     �  '   �     �            1   !     S  #   i     �  
   �     �     �  $   �     �               1         &      4       +      ]   F   /                  *      !          "      A   #   D   J   R      @       V       N       0   K      W   6   >       _   	   '       =   )   M      
   ;          Z   %   [       <      O   b      ,          C      H   Q                  X   ^          B       S       a   G          1   $   :   c   `                  \   9           Y   ?                     P   7          E           8       (   3                         I                -   U   .   L   d      T      5   2    Against DRM License All rights reserved An error occurred during the rendering of this plugin. Application logs Back C band Cancel Clear Confirm Password Content library is currently empty Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Delivery system Discard Download Download application log Error Errors Files Forbidden Free Art License Frequency Frequency must be a positive number GNU Free Documentation License Go up one level Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization LNB Type Language Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Log into Librarian Log out Login Logs are shown in reverse chronological order Modulation Network interfaces Next No No files are being downloaded None North America Ku band Notifications OK Open Content License Open Publication License Other free license Other non-free license Page not found Password Please enter the correct username and password. Please select the satellite you'd like to receive data from. Polarization Public Domain Quality Script output Select a satellite Settings Setup Completed Signal Start using Outernet Symbol rate Symbolrate must be a positive number The entered passwords do not match. The page you were looking for could not be found There are currently no files or folders here. There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. Tuner settings Under maintenance Universal Unknown license Username Vertical Yes You'll be taken to {target} in {seconds} seconds. any language confirm password error here next no lock no output password previous unknown username Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Against DRM License  Все права защищены An error occurred during the rendering of this plugin. Журналы программы Back C band Cancel Clear Confirm Password Библиотека публикаций пуста Creative Commons Attribution  Creative Commons Attribution-NoDerivs  Creative Commons Attribution-NonCommercial  Creative Commons Attribution-NonCommercial-NoDerivs  Creative Commons Attribution-NonCommercial-ShareAlike  Creative Commons Attribution-ShareAlike  Custom... Удалить Delivery system Discard Скачать Download application log Ошибка Oшибки Файлы Запрещено Лицензия Свободного Искусства (Free Art License) Frequency Значение частоты должно быть положительным. GNU Free Documentation License  Go up one level Горизонтальный Неверный выбор для LNB типа. Invalid choice for delivery system Неверный выбор режима модуляции. Invalid choice for polarization LNB Type Language Библиотекарь находится в режиме обслуживания. Пожалуйста, подождите несколько минут и попробуйте снова. Библиотека Загружается... Log into Librarian Выйти Войти Журналы показаны в обратном хронологическом порядке Modulation Сетевые интерфейсы Следующий No No files are being downloaded None Северная Америка Ку-диапазон Notifications OK Open Content License  Open Publication License  Другие свободные лицензии Другие не свободные лицензии страница не найдена Password Пожалуйста, введите действующее имя пользователя и пароль Пожалуйста, выберите спутник, с которого вы хотите получать данные. Polarization Публичный домен Quality Вывод скрипта Выберите спутник Settings Настройка завершина Signal Start using Outernet Symbol rate Значение скорости передачи должно быть положительным. Введенные пароли не совпадают. Страница, которую вы ищете, не может быть найден Здесь пока еще нет файлов или папок. There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. Настройки тюнера Under maintenance Универсальный Неизвестная лицензия Username Вертикальный Yes You'll be taken to {target} in {seconds} seconds. другой язык подтвердите пароль ошибка здесь Следующий разблокирован Нет выходных данных пароль предыдущий неизвестный пользователь 