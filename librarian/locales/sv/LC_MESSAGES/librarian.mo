��    d      <  �   \      �     �     �  6   �     �     �     �     �     	     
	  "   	     >	  %   [	  *   �	  3   �	  5   �	  '   
  	   >
     H
     O
     _
     g
     p
     �
     �
     �
  	   �
     �
  	   �
  #   �
     �
       
          "   ;  "   ^     �     �     �  T   �       
             .     6  -   <  
   j     u     �     �     �     �     �     �     �     �     �               2     A  /   J  <   z     �     �     �     �     �     �                    0  $   <  #   a  0   �  -   �     �             U   -     �     �  	   �     �     �     �     �  1   �               $     *     /     4  	   <     F     O     X     `  "  i     �     �  6   �     �     �          
            ,   (     U  &   r  (   �  2   �  1   �  %   '  	   M     W     ^     n  	   v     �     �     �     �  
   �     �  	   �  '   �     �       
   *     5  "   O  "   r     �     �     �  j   �     0  	   6     @     S     \  *   e  
   �     �     �     �     �     �     �     �     �     �          -     >     W     o  8   x  :   �     �     �     
                2     ;     R     Y     n  0   z  %   �  '   �  0   �     *     I      R  U   s     �     �  	   �     �                 1        O     _     q     u     z  
   �     �  	   �     �     �     �         &      4       +      ]   F   /                  *      !          "      A   #   D   J   R      @       V       N       0   K      W   6   >       _   	   '       =   )   M      
   ;          Z   %   [       <      O   b      ,          C      H   Q                  X   ^          B       S       a   G          1   $   :   c   `                  \   9           Y   ?                     P   7          E           8       (   3                         I                -   U   .   L   d      T      5   2    Against DRM License All rights reserved An error occurred during the rendering of this plugin. Application logs Back C band Cancel Clear Confirm Password Content library is currently empty Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Delivery system Discard Download Download application log Error Errors Files Forbidden Free Art License Frequency Frequency must be a positive number GNU Free Documentation License Go up one level Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization LNB Type Language Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Log into Librarian Log out Login Logs are shown in reverse chronological order Modulation Network interfaces Next No No files are being downloaded None North America Ku band Notifications OK Open Content License Open Publication License Other free license Other non-free license Page not found Password Please enter the correct username and password. Please select the satellite you'd like to receive data from. Polarization Public Domain Quality Script output Select a satellite Settings Setup Completed Signal Start using Outernet Symbol rate Symbolrate must be a positive number The entered passwords do not match. The page you were looking for could not be found There are currently no files or folders here. There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. Tuner settings Under maintenance Universal Unknown license Username Vertical Yes You'll be taken to {target} in {seconds} seconds. any language confirm password error here next no lock no output password previous unknown username Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=2; plural=(n != 1);
 Mot DRM licens Alla rättigheter reserverade An error occurred during the rendering of this plugin. Programloggar Back C band Cancel Clear Confirm Password Innehållsarkivet är för tillfället tomt. Creative Commons Attribution Creative Commons Attribution-NoDerivs  Creative Commons Attribution-IngenReklam Creative Commons Attribution-IngenReklam-NoDerivs  Creative Commons Attribution-IngenReklam-DelaLika Creative Commons Attribution-DelaLika Ändra... Radera Delivery system Discard Ladda ner Ladda ner aplications logg Fel Fel filer Förbjuden Gratis Konst Licens Frequency Frekvensen måste vara ett positivt tal GNU Gratis Dokumentation Licens Go up one level Horisontal Ogiltigt val för LNB typ Ogiltigt val av levererings system Ogiltigt val för moduleringsläge Ogiltigt val for polarisering LNB Type Language Bibliotekarien är för tillfället i underhållsläge. Vänligen vänta några minuter och försök igen. Arkiv Laddar... Log into Librarian Logga ut Logga in Loggar visas i omvänd kronologisk ordning Modulation Nätverksgränssnitt Nästa No Inga filer laddas ner Ingen Nord Amerika ku-band Notifications OK Öppen innehålls licens Öppen offentlig licens Annan fri licens Annan icke-gratis licens Sidan kunde inte hittas Password Vänligen fyll i ett korrekt användarnamn och lösenord Vänligen välj vilken satellit du vill ta emot data från Polarization Offentlig Domän Quality Skript utdata Välj en satellit Settings Installation Slutförd Signal Start using Outernet Symbol rate Teckenhastigheten måste vara ett positivt numer Det ifyllda lösenordet matchar inte. Sidan du letade efter kunde inte hittas Det finns nuvarande inga filer eller mappar här There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. Tuner inställningar Under underhåll Universal Okänd licens Username Vertikal Yes You'll be taken to {target} in {seconds} seconds. Valfritt språk upprepa lösenord fel här nästa inget lås Ingen utdata lösenord föregående okänd användarnamn 