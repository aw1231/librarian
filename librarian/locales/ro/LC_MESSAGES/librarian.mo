��    0      �  C         (     )     =     Q     b  %     *   �  3   �  5     '   :  	   b     l     s     �     �     �     �  #   �     �  
   �     �  "     "   =     `  T   �     �  -   �               &     )     >     W     j     �     �  $   �  -   �     �       	              0     9     ?     D  	   L     V  R  _     �	     �	     �	     �	  %   
  *   5
  3   `
  5   �
  '   �
  	   �
  
   �
                &     ,     5  #   F     j  
   �     �  "   �  "   �     �  T        k  :   w     �     �     �     �     �     �          ,     :  $   M  1   r     �     �  	   �     �     �     �     �       	                 /          -          (              	                   $                )                                              '       
      +                       &   "       0   !          .      #   %   ,            *                 Against DRM License All rights reserved Application logs Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Download application log Error Errors Files Free Art License Frequency must be a positive number GNU Free Documentation License Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Logs are shown in reverse chronological order None North America Ku band OK Open Content License Open Publication License Other free license Other non-free license Public Domain Select a satellite Symbolrate must be a positive number There are currently no files or folders here. Tuner settings Under maintenance Universal Unknown license Vertical error next no lock no output previous Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);
 Against DRM License Toate drepturile rezervate Application logs Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Ștergeți Download application log Error Erori Fișiere Free Art License Frequency must be a positive number GNU Free Documentation License Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Bibliotecă Intrările sunt prezentate în ordine invers cronologică. None North America Ku band Bun Open Content License Open Publication License Other free license Altă licență plătită Public Domain Select a satellite Symbolrate must be a positive number Nu sunt fișiere sau dosare aici în acest moment Tuner settings Under maintenance Universal Licență necunoscută Vertical eroare pagina unmătoare no lock no output pagina precedentă 