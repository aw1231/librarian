��    3      �  G   L      h     i     }     �  "   �     �  %   �  *     3   3  5   g  '   �  	   �     �     �     �     �     �       #        7  
   V     a  "   }  "   �     �  T   �     8  
   @  -   K     y     ~     �     �     �     �     �     �     �       $     -   C     q     �  	   �     �     �     �     �     �  	   �     �  l  �     H
     ]
     q
  (   �
     �
  &   �
  )   �
  5     C   N  5   �  	   �     �     �     �     �             #   $  %   H  
   n     y  "   �  "   �     �  T   �  
   P     [  5   j     �     �     �     �     �     �          '     3     A  $   T  0   y     �     �  	   �     �     �     �     �            	                   !   
      *      1   #               )         3         .                       +               (      %                    ,   $   "      /   '      	                          0   &      2                             -                     Against DRM License All rights reserved Application logs Content library is currently empty Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Download application log Error Errors Files Free Art License Frequency must be a positive number GNU Free Documentation License Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Logs are shown in reverse chronological order None North America Ku band OK Open Content License Open Publication License Other free license Other non-free license Public Domain Script output Select a satellite Symbolrate must be a positive number There are currently no files or folders here. Tuner settings Under maintenance Universal Unknown license Vertical error next no lock no output previous Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Licenca protiv DRM-a Sva prava zadržana Poruke aplikacije Bibilioteka sadržaja je trenutno prazna Creative Commons Autorstvo Creative Commons Autorstvo-Bez prerada Creative Commons Autorstvo-Nekomercijalno Creative Commons Autorstvo-Nekomercijalno-Bez prerada Creative Commons Autorstvo-Nekomercijalno-Deliti pod istim uslovima Creative Commons Autorstovo-Deliti pod istim uslovima Custom... Briši Download application log Error Greške Fajlovi Licenca besplatne umetnosti Frequency must be a positive number GNU licenca za slobodnu dokumentaciju Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Biblioteka Učitavanje... Poruke su prikazane u obrnutom hronološkom redosledu None North America Ku band OK Licenca otvorenog sadržaja Licenca otvorene publikacije Druge slobodne licence Druge neslobodne licence Javni domen Izlaz skripte Select a satellite Symbolrate must be a positive number Trenutno nema fajlova i foldera na ovoj lokaciji Podešavanja tjunera Under maintenance Universal Nepoznata licenca Vertical greška sledeća no lock nema izlaza prethodna 