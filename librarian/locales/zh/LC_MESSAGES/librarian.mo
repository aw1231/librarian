��    c      4  �   L      p     q     �  6   �     �     �     �     �     �     �     	  %   (	  *   N	  3   y	  5   �	  '   �	  	   
     
     
     ,
     4
     =
     V
     \
     c
  	   i
     s
  	   �
  #   �
     �
     �
  
   �
     �
  "     "   +     N     n     w  T   �     �  
   �     �     �       -   	  
   7     B     U     Z     ]     {     �     �     �     �     �     �     �     �       /     <   G     �     �     �     �     �     �     �     �     �     �  $   	  #   .  0   R  -   �     �     �      �  U   �     P     _  	   q     {     �     �     �  1   �     �     �     �     �     �       	   	               %     -    6     U     q  6   ~     �     �     �     �     �     �     �  !   �  '   !  5   I  ;     '   �  	   �  	   �     �                    )     0     7     >     E  	   X     b     x     �  	   �     �     �     �     �     �        6   	     @     G     T     g     n  !   u  
   �     �  	   �     �     �     �     �     �     �     �               -     C     S  '   \  <   �     �     �     �     �     �     �                    2     >  #   W     {  $   �     �     �      �  U        ^  	   n  	   x     �     �  	   �     �  1   �     �     �     �     �  	     	     	        "  	   )     3  	   :         %      3       *      \   E   .                  )          /       !      @   "   C   I   Q      ?       U       M           J      V   5   =       ^   	   &       <   (   L      
   :          Y   $   Z       ;      N   a      +          B      G   P                  W   ]          A       R       `   F          0   #   9   b   _                  [   8           X   >                     O   6          D          7       '   2                         H                ,   T   -   K   c      S      4   1    Against DRM License All rights reserved An error occurred during the rendering of this plugin. Application logs Back C band Cancel Clear Confirm Password Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Delivery system Discard Download Download application log Error Errors Files Forbidden Free Art License Frequency Frequency must be a positive number GNU Free Documentation License Go up one level Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization LNB Type Language Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Log into Librarian Log out Login Logs are shown in reverse chronological order Modulation Network interfaces Next No No files are being downloaded None North America Ku band Notifications OK Open Content License Open Publication License Other free license Other non-free license Page not found Password Please enter the correct username and password. Please select the satellite you'd like to receive data from. Polarization Public Domain Quality Script output Select a satellite Settings Setup Completed Signal Start using Outernet Symbol rate Symbolrate must be a positive number The entered passwords do not match. The page you were looking for could not be found There are currently no files or folders here. There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. Tuner settings Under maintenance Universal Unknown license Username Vertical Yes You'll be taken to {target} in {seconds} seconds. any language confirm password error here next no lock no output password previous unknown username Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: zh-CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=1; plural=0;
 反数字版权管理许可 版权所有 An error occurred during the rendering of this plugin. 应用日志 Back C band Cancel Clear Confirm Password 知识共享署名 知识共享署名 - 禁止演绎 知识共享署名 - 非商业性使用 知识共享署名 - 非商业性使用- 禁止演绎 知识共享署名 - 非商业性使用- 相同方式共享 知识共享署名 - 相同方式共享 定制... 删除？ Delivery system Discard 下载 下载应用日志 错误 错误 文件 禁止 自由艺术许可 Frequency 频率必须为正数 GNU自由文档许可 Go up one level 水平的 无效LNB型号 无效传送系统选项 无效调制选项 无效极化选项 LNB Type Language 文库正在维护中，请等待几分钟后重试。 文库 加载中... Log into Librarian 登出 登陆 按相反时间顺序排列日志 Modulation 网络接口 下一步 No 未下载文件 无 北美Ku波段 Notifications 确定 开放内容许可 开放出版许可 其他自由许可 其他非自由授权 网页未找到 Password 请输入正确的用户名和密码。 Please select the satellite you'd like to receive data from. Polarization 公共领域 Quality 脚本输出 选择卫星 Settings Setup Completed Signal Start using Outernet Symbol rate 码速率必须为正数 The entered passwords do not match. 您查找的网页未能找到 此处目前没有文件或文件夹 There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. 调谐器设置 维护中 通用的 未知授权 Username 垂直的 Yes You'll be taken to {target} in {seconds} seconds. 任何语言 confirm password 错误 这里 后一页 未锁定 无输出 密码 前一页 未知 用户名 