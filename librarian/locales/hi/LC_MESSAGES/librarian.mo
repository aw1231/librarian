��    @        Y         �     �     �     �  "   �     �  %   �  *      3   K  5     '   �  	   �     �     �     �                 	   #     -  #   >     b  
   �     �  "   �  "   �     �  T        c  
   k     v     ~  -   �     �     �     �     �     �     	     	     /	     B	     Y	  /   h	     �	     �	     �	  $   �	  0   �	  -   
     K
     Z
  	   l
     v
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
  "  �
  C   �  7   C  (   {  O   �  F   �  A   ;  ^   }  K   �  Z   (  D   �  	   �     �     �     �          
     &  	   <  ;   F  t   �  T   �  	   L  N   V  \   �  S     R   V  T   �     �          0     8  �   >     �     �     �  8   	  	   B  D   L  J   �  F   �  H   #     l  /   {  ,   �  1   �     
  ]     0   {  ~   �  %   +     Q     c  6        �     �     �     �     �  .   �     '     0     @     H        1      &             #      ?   !                     +                    (   3       %      :   0      =   *         2                                ,   /          -   4                 '   9          .       	   5              6                "          <   8       ;      7   )          
       $   >           @    Against DRM License All rights reserved Application logs Content library is currently empty Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Download Download application log Error Errors Files Forbidden Free Art License Frequency must be a positive number GNU Free Documentation License Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Log out Login Logs are shown in reverse chronological order Network interfaces No files are being downloaded None North America Ku band OK Open Content License Open Publication License Other free license Other non-free license Page not found Please enter the correct username and password. Public Domain Script output Select a satellite Symbolrate must be a positive number The page you were looking for could not be found There are currently no files or folders here. Tuner settings Under maintenance Universal Unknown license Vertical any language error next no lock no output password previous unknown username Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=2; plural=(n != 1);
 DRM के अनुज्ञापत्र के खिलाफ सर्वाधिकार सुरक्षित एप्लिकेशन सूची सामग्री पुस्तकालय अभी खाली है  सृजनशील  सर्वसामान्य रोपण क्रिएटिव कॉमन्स रोपण-NoDerivs क्रिएटिव कॉमन्स रोपण-गैर वाणिज्यिक क्रिएटिव कॉमन्स रोपण गैर-NoDerivs क्रिएटिव कॉमन्स रोपण गैर-d प्रलेखन क्रिएटिव कॉमन्स रोपण- ShareAlike Custom... हटाना Download Download application log Error त्रुटियाँ फ़ाइलें Forbidden मुफ्त कला अनुज्ञापत्र फ्रीक्वेंसी एक सकारात्मक संख्या होनी चाहिए ग्नू मुक्त प्रलेखन अनुज्ञापत्र आड़ा LNB के प्रकार के लिए अवैध विकल्प वितरण प्रणाली के लिए अमान्य विकल्प मॉडुलन मोड के लिए अमान्य विकल्प ध्रुवीकरण के लिए अमान्य विकल्प Librarian is currently in maintenance mode. Please wait a few minutes and try again. पुस्तकालय लोडिंग... Log out Login सूची विपरीत कालानुक्रमिक क्रम में दिखाए जाते हैं Network interfaces No files are being downloaded कोई नहीं उत्तरी अमेरिका Ku बैंड ठीक खुली सामग्री अनुज्ञापत्र ओपेन पब्लिकेशन अनुज्ञापत्र अन्य नि: शुल्क अनुज्ञापत्र अन्य गैर-मुक्त अनुज्ञापत्र Page not found Please enter the correct username and password.  सार्वजनिक डोमेन स्क्रिप्ट उत्पादन Select a satellite Symbolrate एक सकारात्मक संख्या होनी चाहिए The page you were looking for could not be found यहाँ कोई फाइल या फ़ोल्डर वर्तमान में कर रहे हैं। ट्यूनर सेटिंग Under maintenance यूनिवर्सल अज्ञात  अनुज्ञापत्र  सीधा any language त्रुटि अगला no lock कोई उत्पादन नहीं | password पूर्व unknown username 