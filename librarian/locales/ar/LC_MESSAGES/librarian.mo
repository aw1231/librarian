��    d      <  �   \      �     �     �  6   �     �     �     �     �     	     
	  "   	     >	  %   [	  *   �	  3   �	  5   �	  '   
  	   >
     H
     O
     _
     g
     p
     �
     �
     �
  	   �
     �
  	   �
  #   �
     �
       
          "   ;  "   ^     �     �     �  T   �       
             .     6  -   <  
   j     u     �     �     �     �     �     �     �     �     �               2     A  /   J  <   z     �     �     �     �     �     �                    0  $   <  #   a  0   �  -   �     �             U   -     �     �  	   �     �     �     �     �  1   �               $     *     /     4  	   <     F     O     X     `  i  i      �  "   �  6        N     h     m     t     {     �  +   �     �  -   �  [     w   d  m   �  >   J     �     �     �     �  
   �  "   �     �  
   �  
   �  
   �     
  	   )  3   3  )   g     �     �  )   �  6   �  4     /   @     p     y  y   �     �          &     9     B  ,   K  
   x     �     �     �  "   �     �     �     �     �  "         +     L  +   k  %   �     �  N   �  `        v     �     �     �     �     �     �     �     �       <     4   T  C   �  7   �          $      -  U   N     �     �  
   �     �           	       1        L      Z     {     �     �     �     �     �     �     �     �         &      4       +      ]   F   /                  *      !          "      A   #   D   J   R      @       V       N       0   K      W   6   >       _   	   '       =   )   M      
   ;          Z   %   [       <      O   b      ,          C      H   Q                  X   ^          B       S       a   G          1   $   :   c   `                  \   9           Y   ?                     P   7          E           8       (   3                         I                -   U   .   L   d      T      5   2    Against DRM License All rights reserved An error occurred during the rendering of this plugin. Application logs Back C band Cancel Clear Confirm Password Content library is currently empty Creative Commons Attribution Creative Commons Attribution-NoDerivs Creative Commons Attribution-NonCommercial Creative Commons Attribution-NonCommercial-NoDerivs Creative Commons Attribution-NonCommercial-ShareAlike Creative Commons Attribution-ShareAlike Custom... Delete Delivery system Discard Download Download application log Error Errors Files Forbidden Free Art License Frequency Frequency must be a positive number GNU Free Documentation License Go up one level Horizontal Invalid choice for LNB type Invalid choice for delivery system Invalid choice for modulation mode Invalid choice for polarization LNB Type Language Librarian is currently in maintenance mode. Please wait a few minutes and try again. Library Loading... Log into Librarian Log out Login Logs are shown in reverse chronological order Modulation Network interfaces Next No No files are being downloaded None North America Ku band Notifications OK Open Content License Open Publication License Other free license Other non-free license Page not found Password Please enter the correct username and password. Please select the satellite you'd like to receive data from. Polarization Public Domain Quality Script output Select a satellite Settings Setup Completed Signal Start using Outernet Symbol rate Symbolrate must be a positive number The entered passwords do not match. The page you were looking for could not be found There are currently no files or folders here. There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. Tuner settings Under maintenance Universal Unknown license Username Vertical Yes You'll be taken to {target} in {seconds} seconds. any language confirm password error here next no lock no output password previous unknown username Project-Id-Version: Librarian
Report-Msgid-Bugs-To: translations@outernet.is
POT-Creation-Date: 2015-12-09 19:01+0100
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Plural-Forms: nplurals=6; plural= n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
 ضد اولوية الترخيص جميع الحقوق محفوظة An error occurred during the rendering of this plugin. سجلات التطبيق Back C band Cancel Clear Confirm Password محتوى المكتبة فرغ حاليا الإبداعي العزو مشاع مبدع-لايوجد استرجاع المشاع المبدع نسبة المصنف إلى مؤلفه، غير التجارية المشاع المبدع نسبة المصنف إلى مؤلفه، غير التجارية-لايوجد استرجاع المشاع المبدع نسبة المصنف إلى مؤلفه، غير التجارية-شير الايك مشاع مبدع للوثائق الحرة- شير الايك مخصص... مسح Delivery system Discard تنزيل تنزيل ملخص التطبيق خطأ اخطاء ملفات ممنوع ترخيص الفن مجاني Frequency الاشارة يجب ان تكون رقم موجب رخصة جنو للوثائق الحرة Go up one level أفقي أختيار لنوع LNB غير صالح أختيار غير صالح لنظام التوصيل أختيار غير صالح لوضع التعديل أختيار غير صالح للإستقطاب LNB Type Language المكتبي حاليا في حالة الاصلاح. رجاءا انتظر بضع دقائق وجرب مرة أخرى. المكتبة جاري التحميل... Log into Librarian خروج دخول سجلات تظهر في ترتيب عكسي Modulation تقاطعات الشبكة التالي No لم يتم تنزيل اي ملف فارغ North America Ku band Notifications حسنا افتح محويات الرخيص ترخيص مفتوح النشر رخصة مجانية اخرى الرخص الاخرى غير مجانية الصفحة لم يتم ايجاها Password لطفا ادخل اسم المستخدم وكلمة السر الصحيحين الرجاء أختيار القمر الذي ترغب بأستقبال البيانات منه. Polarization المجال العام
 Quality مخرج السكربت أختار القمر Settings تم التنزيل Signal Start using Outernet Symbol rate معدل الترميز يجب أن يكون رقم موجب كلمة السر المدخلة غير مطابقة الصفحة التي فتشت عنها لم يتم ايجادها. لايوجد هُنا أي ملفات أو مجلدات There are no new notifications Timezone Transponder configuration saved. Tuner configuration could not be saved. Please make sure that the tuner is connected. إعدادات المدوزن تحت الاصلاح عالمي رخصة غير معروفه Username عامودي Yes You'll be taken to {target} in {seconds} seconds. اية لغة تأكيد كلمة المرور خطا هنا التالي غير مقفل لايوجد مخرج كلمة السر السابق غير معروف اسم المستخدم 